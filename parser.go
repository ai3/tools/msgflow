package main

import (
	"flag"
	"fmt"
	"log"
	"regexp"
	"strconv"
)

var logUncaught = flag.Bool("log-uncaught", false, "dump log lines that did not match")

// A delivery is when a message is sent somewhere from within a
// specific Flow. Deliveries are uniquely identified by their relay
// attribute (recipients are aggregated by relay). We only keep track
// of one remote Message-ID to simplify the resulting
// tree. Furthermore, we know that each dovecot delivery has a
// different Flow so we can only store the destination mailbox once.
type Delivery struct {
	To       []string
	Relay    string
	RemoteID string
	Mailbox  string
}

func (d *Delivery) addTo(s string) {
	for _, t := range d.To {
		if s == t {
			return
		}
	}
	d.To = append(d.To, s)
}

// A Flow records how a message flows through our services. Each flow
// is associated with a specific process (service), and can have
// multiple deliveries.
type Flow struct {
	Timestamp string
	Host      string
	ID        string
	Instance  string
	RemoteID  string
	From      string
	OrigTo    string

	Deliveries []*Delivery
}

func (f *Flow) addDelivery(to, relay, remoteID, mailbox string) {
	for _, d := range f.Deliveries {
		if d.Relay == relay {
			d.addTo(to)
			return
		}
	}
	f.Deliveries = append(f.Deliveries, &Delivery{
		To:       []string{to},
		Relay:    relay,
		RemoteID: remoteID,
		Mailbox:  mailbox,
	})
}

// SpamResult holds the result of a spamd classification.
type SpamResult struct {
	IsSpam bool
	Score  int
	Tags   string
}

// Message holds information about a specific message, identified by
// its Message-ID. When messages go through the mailman service, they
// are tagged as being associated with a list.
type Message struct {
	ID    string
	Flows []*Flow

	List              string
	MailmanHeldReason string

	// Set spam results by delivery user, to be reconciled in
	// post-processing.
	SpamResults map[string]*SpamResult
}

// Parser eats mail-related logs and builds a detailed map of the
// message flows. It does not particularly care about parsing logs in
// the original time order.
type Parser struct {
	flows    map[string]*Flow
	messages map[string]*Message
}

func NewParser() *Parser {
	return &Parser{

		flows:    make(map[string]*Flow),
		messages: make(map[string]*Message),
	}
}

func (p *Parser) getFlowWithQueueID(id, ts, prog, host string) *Flow {
	f, ok := p.flows[id]
	if !ok {
		f = &Flow{
			ID:        id,
			Instance:  prog,
			Host:      host,
			Timestamp: ts,
		}
		p.flows[id] = f
	}
	return f
}

func (p *Parser) getMessageWithID(id string) *Message {
	m, ok := p.messages[id]
	if !ok {
		m = &Message{
			ID:          id,
			SpamResults: make(map[string]*SpamResult),
		}
		p.messages[id] = m
	}
	return m
}

func associateFlowWithMessage(f *Flow, m *Message) {
	for _, ff := range m.Flows {
		if f.ID == ff.ID {
			return
		}
	}
	m.Flows = append(m.Flows, f)
}

// Matchers are functions that modify the database based on regexp
// matching incoming log lines.
type matchFn func(p *Parser, matches []string, ts, prog, host string)

type matcher struct {
	progRx *regexp.Regexp
	msgRx  *regexp.Regexp
	fn     matchFn
}

func appendIfNotPresent(l []string, s string) []string {
	for _, ll := range l {
		if s == ll {
			return l
		}
	}
	return append(l, s)
}

func doSent(p *Parser, matches []string, ts, prog, host string) {
	queueID := matches[1]
	f := p.getFlowWithQueueID(queueID, ts, prog, host)
	f.OrigTo = matches[4]

	//f.To = appendIfNotPresent(f.To, matches[2])
	//f.Relay = matches[5]

	var remoteID string
	if len(matches) > 8 {
		remoteID = matches[8]
	}

	f.addDelivery(matches[2], matches[5], remoteID, "")
}

func doSieve(p *Parser, matches []string, ts, prog, host string) {
	queueID := matches[2]
	f := p.getFlowWithQueueID(queueID, ts, prog, host)
	f.addDelivery(matches[1], "", "", matches[4])

	id := matches[3]
	m := p.getMessageWithID(id)
	associateFlowWithMessage(f, m)
}

func doMessageID(p *Parser, matches []string, ts, prog, host string) {
	queueID := matches[1]
	id := matches[2]
	f := p.getFlowWithQueueID(queueID, ts, prog, host)
	m := p.getMessageWithID(id)
	associateFlowWithMessage(f, m)
}

func doFrom(p *Parser, matches []string, ts, prog, host string) {
	queueID := matches[1]
	f := p.getFlowWithQueueID(queueID, ts, prog, host)
	f.From = matches[2]
}

func doSpam(p *Parser, matches []string, ts, prog, host string) {
	id := matches[5]
	m := p.getMessageWithID(id)

	score, _ := strconv.Atoi(matches[2])
	m.SpamResults[matches[4]] = &SpamResult{
		IsSpam: (matches[1] == "Y"),
		Score:  score,
		Tags:   matches[3],
	}
}

func doEndFlow(p *Parser, matches []string, ts, prog, host string) {
	//queueID := string(matches[2])
	//delete(flows, queueID)
}

func doMailmanHeld(p *Parser, matches []string, ts, prog, host string) {
	id := matches[3]
	m := p.getMessageWithID(id)
	m.List = matches[1]
	m.MailmanHeldReason = matches[4]
}

func doMailmanDiscarded(p *Parser, matches []string, ts, prog, host string) {
	id := matches[1]
	m := p.getMessageWithID(id)
	m.List = matches[2]
}

func doMailmanPost(p *Parser, matches []string, ts, prog, host string) {
	id := matches[3]
	m := p.getMessageWithID(id)
	m.List = matches[1]
}

func uncaught(matches []string, ts, prog, host string) {
	fmt.Printf("uncaught line: %s\n", string(matches[1]))
}

const qidPrefix = "^([a-zA-Z0-9]{5,}): "

const (
	progPostfix = "^postfix(-.*|$)"
	progSpamd   = "^spamd$"
	progDovecot = "^dovecot$"
	progMailman = "^mailman$"
)

// Ordered set of matchers. The first match breaks the loop.
var matcherSpecs = []struct {
	progRx string
	msgRx  string
	fn     matchFn
}{
	{progPostfix, qidPrefix + `to=<([^,]+)>, (orig_to=<([^,]*)>, )?relay=([^,]*), delay=([0-9.]+), delays=([0-9./]+), .*status=sent \(250 .* (\S+) Saved\)`, doSent},
	{progPostfix, qidPrefix + `to=<([^,]+)>, (orig_to=<([^,]*)>, )?relay=([^,]*), delay=([0-9.]+), delays=([0-9./]+), .*status=sent.*queued as ([a-zA-Z0-9]{5,})`, doSent},
	{progPostfix, qidPrefix + `to=<([^,]+)>, (orig_to=<([^,]*)>, )?relay=([^,]*), delay=([0-9.]+), delays=([0-9./]+), .*status=sent`, doSent},
	{progPostfix, qidPrefix + `message-id=(\S*)`, doMessageID},
	{progPostfix, qidPrefix + `from=<([^>]+)>`, doFrom},
	{progPostfix, qidPrefix + "removed", doEndFlow},

	{progSpamd, `^spamd: result: (.) ([-0-9]*) - (\S*) .*,user=([^,]+).*,mid=([^,]+)`, doSpam},
	//"(spamd[^:]*: spamd: result:.*)$":                                                 uncaught,

	{progDovecot, `^lmtp\(([^\)]+)\)<[0-9]+><([^>]+)>: sieve: msgid=([^:]*):(?: .*:)? stored mail into mailbox '([^']*)'`, doSieve},

	{progMailman, `^(\w+) post from (\S+) held, message-id=([^:]+): (.*)`, doMailmanHeld},
	{progMailman, `^Message discarded, msgid: +([^'#]+)'?#012.*list: ([^,]*),#012`, doMailmanDiscarded},
	{progMailman, `^post to (.+) from ([^,]*).*, message-id=([^,]*), success`, doMailmanPost},
}

func compile() error {
	for _, spec := range matcherSpecs {
		progRx, err := regexp.Compile(spec.progRx)
		if err != nil {
			return fmt.Errorf("error compiling regexp %q: %v", spec.progRx, err)
		}
		msgRx, err := regexp.Compile(spec.msgRx)
		if err != nil {
			return fmt.Errorf("error compiling regexp %q: %v", spec.msgRx, err)
		}
		matchers = append(matchers, &matcher{
			progRx: progRx,
			msgRx:  msgRx,
			fn:     spec.fn,
		})
	}
	return nil
}

// Compiled matchers.
var matchers []*matcher

func init() {
	if err := compile(); err != nil {
		panic(err)
	}
}

func (p *Parser) analyze(ts, host, program, message string) error {
	for i := 0; i < len(matchers); i++ {
		if !matchers[i].progRx.MatchString(program) {
			continue
		}

		m := matchers[i].msgRx.FindStringSubmatch(message)
		if len(m) > 0 {
			matchers[i].fn(p, m, ts, program, host)
			return nil
		}
	}

	if *logUncaught {
		log.Printf("uncaught: %s %s", program, message)
	}
	return nil
}
