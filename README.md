msgflow
===

Little tool to dump the flow of an email message through our
systems. When provided with a Message-ID, or a Postfix message queue
ID, it will print the various steps that the message took while being
delivered through our email service: where it came from, which Postfix
instances it went through, to whom it was ultimately delivered.

This is necessary because every step changes the Postfix queue IDs,
thus making the process of following the path of a message very
painful either via *grep* or even fancier tools like
*Kibana*. Instead, this tool can iteratively query the Elasticsearch
index without intervention from the user.

Refer to the [detailed documentation of the email
service](https://git.autistici.org/ai3/config/-/blob/master/roles/mail/README.md)
to follow more closely.

## How to read the output

Every line in the output consists of a different step in the service
flow, either because the message has moved to another service, or
because it has been delivered externally somewhere. There are some
services which we can't map explicitly in these terms, such as the
mailing list processing service (Mailman), so in that case the
relevant attributes are shown as properties of the message itself.

Example:

```
$ msginfo 4JvQhf6R82z3wh5
Message: <KF2B79A3.0371490@airtelbroadband.in>

   --> 4JvQhf2gPKz1124 (postfix-in@confino) / from=vudythrill@joli.com.au to=debian@autistici.org relay=0.0.0.0[0.0.0.0]:10026
      --> 4JvQhf5l8Yz112T (postfix-out@confino) / from=vudythrill@joli.com.au to=banana@inventati.org orig_to=debian@autistici.org relay=3.mail-backend.investici.org[0.0.0.0]:25
         --> 4JvQhf6R82z3wh5 (postfix-delivery@dipendenza) / from=vudythrill@joli.com.au to=banana@inventati.org relay=3.mail-backend.investici.org[private/dovecot-lmtp]
            --> iP/pH5WmBGJfIi0A89+1pA (dovecot@dipendenza) / to=banana@inventati.org mbox=Spam is_spam=true score=13
```

The above shows a message originally sent to *debian@autistici.org*,
which is an alias of *banana@inventati.org*, as it goes through the
expected *postfix-in* -> *postfix-out* -> *postfix-delivery* ->
*dovecot* services. It is ultimately classified as spam, and delivered
straight to the *Spam* folder.

