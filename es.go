package main

import (
	"context"
	"io"
	"reflect"
	"strings"
	"time"

	"github.com/olivere/elastic/v7"
)

const (
	batchSize   = 100
	objType     = "_doc"
	indexFormat = "logstash-2006.01.02"
)

func getIndexes(from, to time.Time) []string {
	var idxs []string
	t := time.Date(from.Year(), from.Month(), from.Day(), 0, 0, 0, 0, time.UTC)
	for t.Before(to) {
		idxs = append(idxs, t.Format(indexFormat))
		t = t.AddDate(0, 0, 1)
	}
	return idxs
}

func makeQuery(q string, from, to time.Time) (elastic.Query, []string) {
	query := elastic.NewBoolQuery().Must(
		elastic.NewMatchQuery("_type", objType),
		elastic.NewRangeQuery("@timestamp").From(from).To(to),
		elastic.NewMatchPhraseQuery("message", q),
	)
	return query, getIndexes(from, to)
}

type logResult struct {
	Timestamp string `json:"@timestamp"`
	Host      string `json:"host"`
	Program   string `json:"program"`
	Message   string `json:"message"`
}

func runQuery(ctx context.Context, client *elastic.Client, query elastic.Query, indexes []string, fn func(string, string, string, string) error) error {
	// Set up a ScrollService just in case the response is big.
	scroll := client.Scroll(indexes...).
		Query(query).
		Sort("@timestamp", true).
		Size(batchSize)
	defer scroll.Clear(ctx)

	for {
		result, err := scroll.Do(ctx)
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}

		var lm logResult
		for _, hit := range result.Each(reflect.TypeOf(lm)) {
			msg := hit.(logResult)
			if err := fn(
				msg.Timestamp,
				msg.Host,
				msg.Program,
				// Remove the leading whitespace in the message
				// (since it likely originates from syslog).
				strings.TrimPrefix(msg.Message, " "),
			); err != nil {
				return err
			}
		}
	}

}
