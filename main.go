package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/olivere/elastic/v7"
)

var (
	elasticURL = flag.String("url", getenv("ELASTICSEARCH_URL", "http://127.0.0.1:9200"), "Elasticsearch URL")
	days       = flag.Int("days", 14, "how many days to look back")
)

func getenv(k, dflt string) string {
	if s := os.Getenv(k); s != "" {
		return s
	}
	return dflt
}

const maxQueries = 100

func scanID(ctx context.Context, parser *Parser, client *elastic.Client, msgID string) error {
	toT := time.Now()
	fromT := toT.AddDate(0, 0, -(*days))

	query, indexes := makeQuery(msgID, fromT, toT)
	return runQuery(
		ctx,
		client,
		query,
		indexes,
		parser.analyze,
	)
}

func scanMsgInfo(ctx context.Context, parser *Parser, client *elastic.Client, seed string) error {
	// Seed the scan with the root queue ID.
	if err := scanID(ctx, parser, client, seed); err != nil {
		return err
	}

	// Don't really need to track queue IDs and message IDs
	// separately, they're just search queries.
	seen := map[string]struct{}{
		seed: struct{}{},
	}

	// We run incremental discovery by looking at new message IDs
	// or queue IDs that were found by the latest query, until
	// there are no more (or the maximum number of queries has
	// been reached).
	var done bool
	for i := 0; i < maxQueries && !done; i++ {
		// Look at the current seen message IDs / queue IDs
		// and find the new ones.
		done = true

		var newIDs []string
		for qid := range parser.flows {
			if _, ok := seen[qid]; ok {
				continue
			}
			done = false
			newIDs = append(newIDs, qid)
		}
		for msgid := range parser.messages {
			if _, ok := seen[msgid]; ok {
				continue
			}
			done = false
			newIDs = append(newIDs, msgid)
		}

		for _, id := range newIDs {
			if err := scanID(ctx, parser, client, id); err != nil {
				return err
			}
			seen[id] = struct{}{}
		}
	}
	return nil
}

func flowsByID(flows []*Flow) map[string]*Flow {
	byID := make(map[string]*Flow)
	for _, f := range flows {
		byID[f.ID] = f
	}
	return byID
}

type edge struct {
	src, dst string
}

type edgeList []edge

func (l edgeList) children(id string) []string {
	var out []string
	for _, e := range l {
		if e.src == id {
			out = append(out, e.dst)
		}
	}
	return out
}

func calcEdges(flows []*Flow) edgeList {
	var edges []edge

	for _, f := range flows {
		for _, d := range f.Deliveries {
			if d.RemoteID != "" {
				edges = append(edges, edge{src: f.ID, dst: d.RemoteID})
			}
		}
	}

	return edgeList(edges)
}

func entrypoints(flows []*Flow) []string {
	tmp := make(map[string]struct{})
	for _, f := range flows {
		for _, d := range f.Deliveries {
			if d.RemoteID != "" {
				tmp[d.RemoteID] = struct{}{}
			}
		}
	}

	var eps []string
	for _, f := range flows {
		if _, ok := tmp[f.ID]; !ok {
			eps = append(eps, f.ID)
		}
	}
	return eps
}

// Struct to sort flows by their timestamp.
type flowTSSort struct {
	flows map[string]*Flow
	ids   []string
}

func (s *flowTSSort) Len() int      { return len(s.ids) }
func (s *flowTSSort) Swap(i, j int) { s.ids[i], s.ids[j] = s.ids[j], s.ids[i] }
func (s *flowTSSort) Less(i, j int) bool {
	return s.flows[s.ids[i]].Timestamp < s.flows[s.ids[j]].Timestamp
}

func sortFlowsByTimestamp(flows map[string]*Flow, ids []string) {
	s := flowTSSort{flows: flows, ids: ids}
	sort.Sort(&s)
}

func printFlow(m *Message, f *Flow, prefix string) {
	for _, d := range f.Deliveries {
		var args []string
		if f.From != "" {
			args = append(args, fmt.Sprintf("from=%s", f.From))
		}
		if s := strings.Join(d.To, ","); s != "" {
			args = append(args, fmt.Sprintf("to=%s", s))
		}
		if f.OrigTo != "" {
			args = append(args, fmt.Sprintf("orig_to=%s", f.OrigTo))
		}
		if d.Relay != "" {
			args = append(args, fmt.Sprintf("relay=%s", d.Relay))
		}
		if d.Mailbox != "" {
			args = append(args, fmt.Sprintf("mbox=%s", d.Mailbox))
		}

		// Print spam summary on dovecot deliveries.
		if f.Instance == "dovecot" {
			for _, to := range d.To {
				if spam, ok := m.SpamResults[to]; ok {
					args = append(args, fmt.Sprintf("is_spam=%v", spam.IsSpam))
					if spam.IsSpam {
						args = append(args, fmt.Sprintf("score=%d", spam.Score))
					}
				}
			}
		}

		fmt.Printf("%s--> %s (%s@%s) / %s\n", prefix, f.ID, f.Instance, f.Host, strings.Join(args, " "))
	}
}

func recurseDumpFlows(m *Message, flows map[string]*Flow, edges edgeList, id, prefix string) {
	f := flows[id]
	if f == nil {
		// External queue ID.
		return
	}
	printFlow(m, f, prefix)

	for _, chid := range edges.children(id) {
		recurseDumpFlows(m, flows, edges, chid, prefix+"   ")
	}
}

func dumpMessages(parser *Parser) {
	for _, m := range parser.messages {
		fmt.Printf("Message: %s", m.ID)
		if m.List != "" {
			fmt.Printf(" (list=%s)", m.List)
			if m.MailmanHeldReason != "" {
				fmt.Printf(" - held: %s", m.MailmanHeldReason)
			}
		}
		fmt.Printf("\n\n")

		fbid := flowsByID(m.Flows)
		edges := calcEdges(m.Flows)
		eps := entrypoints(m.Flows)
		sortFlowsByTimestamp(fbid, eps)
		for _, id := range eps {
			recurseDumpFlows(m, fbid, edges, id, "   ")
		}
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if flag.NArg() != 1 {
		log.Fatal("wrong number of arguments")
	}
	msgID := flag.Arg(0)

	client, err := elastic.NewClient(elastic.SetURL(*elasticURL))
	if err != nil {
		log.Fatal(err)
	}

	parser := NewParser()

	if err := scanMsgInfo(context.Background(), parser, client, msgID); err != nil {
		log.Fatal(err)
	}

	dumpMessages(parser)
}
